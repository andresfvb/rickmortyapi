import React, {useContext, useEffect} from 'react'
import styles from '../styles/personajes.module.css'
import Image from 'next/image'
import Link from 'next/link'
import PersonajesContext from '../context/PersonajesApi/PersonajesContext'



export const Personajes = ({personaje}) => {

  const {id, name, image, status} = personaje
  return (
    <Link key={id} href={`/personaje?id=${id}`} as={`/personaje?id=${id}`} className={styles.card}>
      <div className={styles.profile}>
        <figure className={styles.cardimg}>
          <Image 
            src={image} 
            width={300} 
            height={50} 
            alt={`Imagen del personaje ${name}`} 
          />
        </figure>
        
        <div className={styles.content}>
          <div className={styles.section}>
            <h2 className={styles.name}>{name}</h2>
            <p className={`estado ${
              status=== "Alive" ? "live" : "die"
              }`}>{`${
                status=== "Alive" ? "Live" : "Dead"
                }`}</p>
            <p> 
              Ver más
            </p>
          </div>
        </div>
      </div>
    </Link>
  )
}






export default Personajes
