import '../styles/globals.css'
import PersonajeState from '../context/PersonajesApi/PersonajeState'

function MyApp({ Component, pageProps }){
    return (
        <PersonajeState>
            <Component {...pageProps}/>
        </PersonajeState>

            
    )
}

export default MyApp