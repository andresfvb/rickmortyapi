import React, {  useEffect, useContext, useState } from 'react';
import Layouts from '../components/layouts';
import Grid from '../components/grids';
import PersonajesContext from '../context/PersonajesApi/PersonajesContext';
import Personajes from '../components/personajes';
import Pagination from '../components/pagination';



const Index = () => {

  const {personajes, getGrupoPersonajes,paginations, pagActual} = useContext(PersonajesContext)
  const [currentPage, setCurrentPage] = useState(pagActual)
  const { pages } = paginations
  const pageSize = pages
  const onPageChange = (page) => {
    setCurrentPage(page)
  }
  
  useEffect(() => {
    currentPage > 1 ? getGrupoPersonajes(currentPage) : getGrupoPersonajes()    
  }, [currentPage]);
  console.log(currentPage);
  return (
    <>
      <Layouts
        title={'Inicio'}
        description={'Página de personajes de Rick and Morty'}
      >
        <main className="screen">
          
            <Grid
              titulo="Personajes principales"
              personajes={personajes}
              Pagina = {Personajes}
            />
            <div>
              <Pagination 
                pag = {paginations}
                currentPage = {currentPage}
                pageSize = {pageSize}
                onPageChange = {onPageChange}
              />
            </div>
        </main>
      </Layouts>
    </>
  );
};

export default Index;
