import Link from 'next/link'
import React from 'react'
import styles from '../styles/pagination.module.css'


const Pagination = ({pag, currentPage, onPageChange, pageSize }) => {


  return (
    <div className={styles.containerPagination}>
        <ul className={styles.pagination}>
          
          {
            currentPage > 1 ? <li className={styles.paginationPrev} onClick={()=>onPageChange(currentPage-1)}>Prev</li> :<li  className={styles.paginationPrevDisabled}>Prev</li>
          }
           
          {
            currentPage < pageSize ? <li className={styles.paginationNext} onClick={()=>onPageChange(currentPage+1)}>Next</li>:<li className={styles.paginationNextDisabled}>Next</li>
          }
        </ul>
    </div>
  )
}

export default Pagination