import React from 'react'
import { GET_PERSONAJE, GET_GLOBAL_PERSONAJES } from '../type'

export default (state, action) => {
  const {payload, type, pag, cargarActual} = action

  switch (type) {
    case GET_GLOBAL_PERSONAJES:

      return{
        ...state,
        personajes:payload,
        paginations: pag,
        actual: cargarActual
      }
    case GET_PERSONAJE:
      // const value = personajes.find(element => element.id === Number(payload))
      return{
        ...state,
        // selectedPersonaje: value
        selectedPersonaje: payload
      }
  
    default:
      return state
      break;
  }

}