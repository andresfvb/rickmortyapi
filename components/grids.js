import React from 'react'
import styles from '../styles/grid.module.css'



const Grid = ({titulo, personajes, Pagina}) => {
  
  return (
    <>
      <h2 className='heading'>{titulo}</h2>
      <div className={styles.personajes}>
      {
      personajes.map((personaje, index) => (
        <Pagina
          personaje = {personaje}
          key = {index}
      />
        
      ))}
        </div>
        
    </>
  )
}

export default Grid

