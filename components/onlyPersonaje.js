import React from 'react'
import styles from '../styles/personaje.module.css'
import Image from 'next/image'
import Breadcrumbs from './pagination'
import Link from 'next/link'


const OnlyPersonaje = ({personaje}) => {

  const { id, name, image, species, gender, status } = personaje
  return (
    <div key={id} className={styles.profile} style={{
      backgroundImage: `url(${image})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: '0px 40%'
    }}>
      <div className={styles.content} style={{
        }}>
          <Image src={image} width={150} height={150} alt={`Imagen del personaje ${name}`} />
        <div className={styles.information}>
          <div className={styles.professional}>
              <div className={styles.title}>
                <h1 className={styles.name}>{name}</h1>
               
              </div>
            </div>
            <div className={styles.personal}>
              <p className={`estado ${
                    status=== "Alive" ? "live" : "die"
                    }`}>{`${
                      status=== "Alive" ? "Live" : "Dead"
                      }`}</p>
              <p>Especie: <span>{species}</span></p>
              <p>Gender: <span>{gender}</span></p>
              <Link href="/">Ver Personajes</Link>
            </div>
        </div>
      </div>
    </div>
  )
}

export default OnlyPersonaje