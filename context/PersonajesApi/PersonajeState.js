import React, {createContext,useReducer, useContext, useEffect, useState} from 'react'
import PersonajesContext from './PersonajesContext';
import PersonajesReducer from './PersonajesReducer';
import axios from 'axios';


const PersonajeState = (props) => {
  const initialState = {
    personajes: [],
    selectedPersonaje: [],
    paginations: [], 
    actual: 1
  }
  const [state, dispatch] = useReducer(PersonajesReducer, initialState)
  const personajesMap = new Map()


  const getGrupoPersonajes = async (number=state.actual) => {
    try {
      const url = await axios.get('https://rickandmortyapi.com/api/character?page=' + number)
      const respuesta =  await url.data
      console.log(respuesta);
      dispatch({
        type: 'GET_GLOBAL_PERSONAJES',
        payload: respuesta.results,
        pag: respuesta.info,
        cargarActual: number
      })
      } catch (error) {
          console.error(error);
      }
  }
  const getPersonaje = async (id) => {
    try {
      const url = 'https://rickandmortyapi.com/api/character/' + id;
      const respuesta = await fetch(url);
  
      if (!respuesta.ok) {
        throw new Error('Error al cargar los datos');
      }
      const datos = await respuesta.json();
      dispatch({
        type: 'GET_PERSONAJE',
        payload: datos
      })
      } catch (error) {
          console.log(error);
        }
  }

 
  // const getPersonaje = async (id) => {
  //   try{
  //     console.log("entre");
  //     const personajes = state.personajes      
  //     dispatch({
  //       type: 'GET_PERSONAJE',
  //       payload: id,
  //       personajes
  //     })}catch(error){
  //       console.log(error);
  //     }
  // }
  return (
    <PersonajesContext.Provider 
    value={{
      personajes: state.personajes,
      selectedPersonaje: state.selectedPersonaje,
      paginations: state.paginations,
      pagActual:state.actual,
      getGrupoPersonajes,
      getPersonaje
    }}>
      {props.children}
    </PersonajesContext.Provider>
  )
}

export default PersonajeState