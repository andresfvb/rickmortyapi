import React, {useContext, useEffect} from 'react'
import { useRouter } from 'next/router';
import Layouts from '../components/layouts';
import PersonajesContext from '../context/PersonajesApi/PersonajesContext';
import Grid from '../components/grids';
import OnlyPersonaje from '../components/onlyPersonaje';

const personaje = () => {
  const router = useRouter()
  const {id} = router.query

    const {selectedPersonaje, getPersonaje, personaje} = useContext(PersonajesContext)
    useEffect(() => {
      id === undefined ? alert("Estoy vacio"):getPersonaje(id)
    }, [id]);
  return (
    <>
        <Layouts
            title={selectedPersonaje.name}
            description={`Personaje - ${selectedPersonaje.name}`}
        >
        { <main className="screen">
          <Grid
            personajes={[selectedPersonaje]}
            Pagina = {OnlyPersonaje}
          />
        </main> }
        </Layouts>
    </>
  )
}

export default personaje